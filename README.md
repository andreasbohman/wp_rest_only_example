# WP REST API Example

Simple example on a headless theme using [WP REST API v2](http://v2.wp-api.org/) and [Advanced Custom Fields Plugin](https://www.advancedcustomfields.com/). Check functions.php in headless-wprestapi-example theme for a simple example on connecting ACF to an API route.