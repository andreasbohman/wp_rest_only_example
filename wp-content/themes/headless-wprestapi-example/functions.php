<?php

//Register a rest field called 'acf' for the 'guide' custom post type
add_action( 'rest_api_init', 'register_guide' );
function register_guide() {
    register_rest_field( 'guide',
        'acf',
        array(
            'get_callback'    => 'get_acf_fields', //reference function below
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_acf_fields( $object, $field_name, $request ) {
    //Get all ACF fields for the current post
    return get_fields($object[ 'id' ]);
}



function theme_setup(){
	add_theme_support( 'post-thumbnails' );

}
add_action( 'after_setup_theme', 'theme_setup' );

/**
   * Register a guide post type, with REST API support
   *
   * Based on example at: http://codex.wordpress.org/Function_Reference/register_post_type
   */
  add_action( 'init', 'my_guide_cpt' );
  function my_guide_cpt() {
  	$labels = array(
  		'name'               => _x( 'Guides', 'post type general name', 'your-plugin-textdomain' ),
  		'singular_name'      => _x( 'Guide', 'post type singular name', 'your-plugin-textdomain' ),
  		'menu_name'          => _x( 'Guides', 'admin menu', 'your-plugin-textdomain' ),
  		'name_admin_bar'     => _x( 'Guide', 'add new on admin bar', 'your-plugin-textdomain' ),
  		'add_new'            => _x( 'Add New', 'guide', 'your-plugin-textdomain' ),
  		'add_new_item'       => __( 'Add New Guide', 'your-plugin-textdomain' ),
  		'new_item'           => __( 'New Guide', 'your-plugin-textdomain' ),
  		'edit_item'          => __( 'Edit Guide', 'your-plugin-textdomain' ),
  		'view_item'          => __( 'View Guide', 'your-plugin-textdomain' ),
  		'all_items'          => __( 'All Guides', 'your-plugin-textdomain' ),
  		'search_items'       => __( 'Search Guides', 'your-plugin-textdomain' ),
  		'parent_item_colon'  => __( 'Parent Guides:', 'your-plugin-textdomain' ),
  		'not_found'          => __( 'No guides found.', 'your-plugin-textdomain' ),
  		'not_found_in_trash' => __( 'No guides found in Trash.', 'your-plugin-textdomain' )
  	);
  
  	$args = array(
  		'labels'             => $labels,
  		'description'        => __( 'Description.', 'your-plugin-textdomain' ),
  		'public'             => true,
  		'publicly_queryable' => true,
  		'show_ui'            => true,
  		'show_in_menu'       => true,
  		'query_var'          => true,
  		'rewrite'            => array( 'slug' => 'guide' ),
  		'capability_type'    => 'post',
  		'has_archive'        => true,
  		'hierarchical'       => false,
  		'menu_position'      => null,
  		'show_in_rest'       => true,
  		'rest_base'          => 'guides-api', //REST API specific
  		'rest_controller_class' => 'WP_REST_Posts_Controller', //REST API specific
  		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
  	);
  
  	register_post_type( 'guide', $args );
}


?>